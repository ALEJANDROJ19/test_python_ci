# Python GitLab CI test 
[![pipeline status](https://gitlab.com/ALEJANDROJ19/test_python_ci/badges/master/pipeline.svg)](https://gitlab.com/ALEJANDROJ19/test_python_ci/commits/master)

[Documentation](https://henningtimm.gitlab.io/post/first_steps_with_gitlabci/)
https://docs.gitlab.com/ee/ci/yaml/

## Notes

+ The CI follows the pipeline as is specified in `stages`.
+ Jobs that belongs to the same `stage` are executed in *parallel*.
+ If all the jobs on a *previous* stage are *successful*, next stage is run.
+ If all the pipeline is correct, the project passes the CI.